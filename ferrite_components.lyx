#LyX 2.2 created this file. For more info see http://www.lyx.org/
\lyxformat 508
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass article
\begin_preamble
\usepackage{booktabs} 
\usepackage{tikz}
\usetikzlibrary{calc}
\usepackage{pgfplots}
\usepgfplotslibrary{groupplots}
\usepgfplotslibrary{units}
\end_preamble
\use_default_options true
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family sfdefault
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\float_placement tph
\paperfontsize default
\spacing double
\use_hyperref false
\papersize a4paper
\use_geometry true
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\leftmargin 3cm
\topmargin 3cm
\rightmargin 3cm
\bottommargin 3cm
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
Fundamental Equations of Ferrite Components Design
\end_layout

\begin_layout Standard
\align center

\shape italic
Author: Michael Heidinger
\end_layout

\begin_layout Standard
\align center

\shape italic
Karlsruhe Institute of Technology (KIT), Light Technology Institute (LTI),
 76131 Karlsruhe, Germany 
\end_layout

\begin_layout Standard
\begin_inset VSpace 3cm
\end_inset


\end_layout

\begin_layout Standard
\align center
\begin_inset Tabular
<lyxtabular version="3" rows="5" columns="2">
<features tabularvalignment="middle">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<row>
<cell multicolumn="1" alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Revision History
\end_layout

\end_inset
</cell>
<cell multicolumn="2" alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Author:
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Michael Heidinger
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Status
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
writing
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Compile Date
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset ERT
status open

\begin_layout Plain Layout

{
\backslash
today}
\end_layout

\end_inset


\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Review Status
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
unreviewed.
 Handle information with care.
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Newpage newpage
\end_inset


\end_layout

\begin_layout Section
Units and their analogies
\end_layout

\begin_layout Standard
The following analogies of the magnetic circuit to the electrical circuit
 can be drawn.
 
\end_layout

\begin_layout Standard
The magnetic voltage is similar to an electric voltage.
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{equation}
U_{E}\sim U_{M}\label{eq:u-analogie}
\end{equation}

\end_inset


\end_layout

\begin_layout Standard
The electric current is similar to the flux 
\begin_inset Formula $\Phi$
\end_inset

.
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{equation}
I_{E}\sim\Phi\label{eq:i-analogie}
\end{equation}

\end_inset


\end_layout

\begin_layout Standard
The electric resistance is similar to the magnetic resistance 
\begin_inset Formula $R_{\mathrm{M}}.$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{equation}
R_{E}\sim R_{M}\label{eq:r-analogie}
\end{equation}

\end_inset


\end_layout

\begin_layout Standard
The flux density is similar to the current density in an electric circuit.
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{equation}
j\sim B\label{eq:j-analogie}
\end{equation}

\end_inset


\end_layout

\begin_layout Standard
The number of windings is called:
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{equation}
N\label{eq:n}
\end{equation}

\end_inset


\end_layout

\begin_layout Standard
The crosssectional area in 
\begin_inset Formula $\mathrm{m^{2}}$
\end_inset

 is called
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{equation}
A\label{eq:n-1}
\end{equation}

\end_inset


\end_layout

\begin_layout Subsection
Connection
\end_layout

\begin_layout Standard
All connections are equal.
 A series and a parallel connection operate in the same manner as in an
 electrical circuit.
\end_layout

\begin_layout Section
Fundamental equations
\end_layout

\begin_layout Standard
The inductance 
\begin_inset Formula $L$
\end_inset

 can be calculated using the 
\begin_inset Formula $Al$
\end_inset

 value given in the ferrite datasheet
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{equation}
L=Al\,N^{2}\label{eq:ind-al}
\end{equation}

\end_inset


\end_layout

\begin_layout Standard
The inductance can be calculated out of the reluctance (magnetic resistance)
 
\begin_inset Formula $R$
\end_inset

.
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{equation}
L=\frac{1}{R}N^{2}\label{eq:ind-r}
\end{equation}

\end_inset


\end_layout

\begin_layout Standard
Based on the previous equations (
\begin_inset CommandInset ref
LatexCommand ref
reference "eq:ind-al"

\end_inset

) and (
\begin_inset CommandInset ref
LatexCommand ref
reference "eq:ind-r"

\end_inset

), the following equation can be formulated:
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{equation}
Al=\frac{1}{R}\label{eq:al-r}
\end{equation}

\end_inset


\end_layout

\begin_layout Section
Flux equations
\end_layout

\begin_layout Standard
The mangetic flux can be calculated according to the following equations.
\begin_inset Formula 
\begin{equation}
\Phi=\frac{N\,I}{R}\label{eq:flux-by-mangetic resistance}
\end{equation}

\end_inset


\end_layout

\begin_layout Standard
The flux density can be calculated by division through the area:
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{equation}
B=\frac{\Phi}{A}\label{eq:flux-by-mangetic resistance-1}
\end{equation}

\end_inset


\end_layout

\begin_layout Section
Limiting Factors in Transformer Design
\end_layout

\begin_layout Standard
There are the following limiting factors on inductor design:
\end_layout

\begin_layout Itemize

\series bold
Flux Density: 
\series default
The maximum flux density may not be exceeded.
 The typical flux density used for conservative design is 
\begin_inset Formula $B=0.35\mathrm{T}$
\end_inset

.
 The maximum allowable flux is typically reduced with increased temperature.
\end_layout

\begin_layout Itemize

\series bold
Winding area: 
\series default
Inductive components have a limited winding area.
\end_layout

\begin_layout Itemize

\series bold
Frequency: 
\series default
The material must be able to handle (high) switching frequency.
 Low class material (e.g.
 N27) may handle 
\begin_inset Formula $f=50kHz$
\end_inset

, while high frequency material may handle 
\begin_inset Formula $f=500kHz$
\end_inset


\end_layout

\begin_layout Section
Design of storage inductors
\end_layout

\begin_layout Standard
There are two limiting factors on
\end_layout

\begin_layout Standard
To be written
\end_layout

\begin_layout Section
Transformer model
\end_layout

\begin_layout Standard
The transformer model is shown in Figure 
\begin_inset CommandInset ref
LatexCommand ref
reference "fig:Transformer-model-magnetic"

\end_inset

.
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
image was wrong
\begin_inset Caption Standard

\begin_layout Plain Layout
\begin_inset CommandInset label
LatexCommand label
name "fig:Transformer-model-magnetic"

\end_inset

Transformer model
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
To fill the transformer, the following mesurements are made:
\end_layout

\begin_layout Enumerate
Primary side measured / Secondary Side open, Resulting in R1o
\end_layout

\begin_layout Enumerate
Primary side open / Secondary Side measured, Resulting in R2o.
\end_layout

\begin_layout Enumerate
Primary side measured / Secondary Side short circuit, Result in R1s.
\end_layout

\begin_layout Section
Design of transformers
\end_layout

\begin_layout Standard
To be written
\end_layout

\begin_layout Section
Additional information
\end_layout

\begin_layout Subsection
Website resources
\end_layout

\begin_layout Itemize
http://www.vias.org/matsch_capmag/matsch_caps_magnetics_chap4_05.html
\end_layout

\begin_layout Subsection
Video lectures
\end_layout

\begin_layout Itemize
https://www.coursera.org/lecture/electronic-converters/reluctance-and-magnetic-cir
cuits-8MPtI
\end_layout

\end_body
\end_document
